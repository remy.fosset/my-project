package rft.project.game;

import io.vavr.collection.List;
import io.vavr.control.Option;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
class Fight {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Fight.class);

    private Team team1;

    private Team team2;

    Option<String> go() {

        printState();
        while (!team1.isAllDead() && !team2.isAllDead()) {

            Option<Character> character1 = team1.findCharacterTurn();
            Option<Character> character2 = team2.findCharacterTurn();

            if (character1.isDefined() && character2.isDefined()) {
                if (character1.get().getCurrentSpeed() > character2.get().getCurrentSpeed()) {
                    character1.get().play(team2);
                    printState();
                } else {
                    character2.get().play(team1);
                    printState();
                }
            } else if (character1.isDefined()) {
                character1.get().play(team2);
                printState();
            } else if (character2.isDefined()) {
                character2.get().play(team1);
                printState();
            } else {
                nextStep();
            }
        }

        return winner();
    }

    private void printState() {
        log.info("------------------------");
        log.info("Fight state");
        log.info("------------------------");
        log.info("Team " + team1.getName());
        team1.getCharacters().filter(Character::isAlive)
                .forEach(c -> log.info(c.toString()));
        log.info("------------------------");
        log.info("Team " + team2.getName());
        team2.getCharacters().filter(Character::isAlive)
                .forEach(c -> log.info(c.toString()));
    }

    private Option<String> winner() {

        List<Team> teamsAlive = List.of(team1, team2).filter(team -> !team.isAllDead());

        return !teamsAlive.isEmpty() ? Option.of(teamsAlive.get(0).getName()) : Option.none();
    }

    private void nextStep() {
        List.ofAll(team1.getCharacters())
                .appendAll(team2.getCharacters())
                .forEach(c -> c.setCurrentSpeed(c.getCurrentSpeed() + c.getSpeed()));
    }
}

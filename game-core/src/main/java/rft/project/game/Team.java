package rft.project.game;

import io.vavr.collection.List;
import io.vavr.control.Option;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Comparator;

@Getter
@Setter
@Builder
class Team {

    public static final String STRING_EMPTY = "";
    public static final String STRING_SPACE = " ";
    private String name;
    private List<Character> characters;

    boolean isAllDead() {
        return characters.filter(Character::isAlive).isEmpty();
    }

    Option<Character> findCharacter(String name) {
        return characters.filter(c -> c.getName().equals(name))
                .toOption();
    }

    Option<Character> findCharacterTurn() {
        return characters
                .filter(Character::isAlive)
                .filter(Character::canPlay)
                .maxBy(Comparator.comparing(Character::getCurrentSpeed));
    }
}

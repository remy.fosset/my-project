package rft.project.game;

import io.vavr.control.Option;
import lombok.Getter;
import lombok.Setter;
import org.apache.log4j.Logger;

import java.util.Scanner;

@Getter
@Setter
class Character {

    private static final Logger log = Logger.getLogger(Character.class);

    private String name;
    private int healthPoint;
    private int currentHealthPoint;
    private int strength;
    private int speed;
    private int currentSpeed;


    Character(String name) {
        this.name = name;
    }

    Character(String name, int healthPoint, int strength, int speed) {
        this.name = name;
        this.healthPoint = healthPoint;
        this.currentHealthPoint = healthPoint;
        this.strength = strength;
        this.speed = speed;
        this.currentSpeed = speed;
    }

    boolean isAlive() {
        return currentHealthPoint > 0;
    }

    private void takeDamage(int enemyStrength) {
        log.debug("Character " + name + " take " + enemyStrength + " damages");
        currentHealthPoint -= enemyStrength;
    }

    private void attack(Character character) {

        character.takeDamage(strength);
        setCurrentSpeed(getCurrentSpeed() - 100);
    }

    void play(Team team) {

        if (canPlay()) {
            log.info("------------------------");
            log.info("Character " + this.toString() + " play");
            attack(chooseCharacterToAttack(team));
        }
    }

    private Character chooseCharacterToAttack(Team team) {

        log.info("------------------------");
        log.info("choose the character to attack");
        team.getCharacters()
                .filter(Character::isAlive)
                .forEach(character -> log.info(character.toString()));

        Scanner sc = new Scanner(System.in);
        String namePlayerToAttack = sc.nextLine();

        Option<Character> characterToAttack = team.findCharacter(namePlayerToAttack);
        if (characterToAttack.isDefined()) {
            return characterToAttack.get();
        } else {
            log.error("character NOT FIND !!!");
            return this.chooseCharacterToAttack(team);
        }
    }

    boolean canPlay() {
        return getCurrentSpeed() > 99;
    }

    public String toString() {
        return name + "(" + currentHealthPoint + "hp)";
    }
}

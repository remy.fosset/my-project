package rft.project.game;

import io.vavr.collection.List;
import org.apache.log4j.Logger;

public class Main {

    private static final Logger log = Logger.getLogger(Main.class);

    public static void main(String[] args) {

        Character grosMinet = new Character("GrosMinet", 12, 4, 55);
        Character tom = new Character("Tom", 10, 4, 65);
        Team chats = Team.builder().characters(List.of(grosMinet, tom)).name("CHAT").build();

        var titi = new Character("Titi", 7, 7, 80);
        Character jerry = new Character("Jerry", 8, 6, 80);
        Team petits = Team.builder().characters(List.of(titi, jerry)).name("PETIT").build();

        log.info("Winner is: " + new Fight(chats, petits).go().getOrElse("No Winner Double KO !"));

    }
}
